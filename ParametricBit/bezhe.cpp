#include <bezhe.h>
#include <math.h>
#include <stdio.h>

// Функция получает точку кривой безье
CanvasPoint pointBezhe(CanvasPoint *array, int n, double t)
{
    if (n == 2)
    {
        Vector2i v = {array[1].x-array[0].x, array[1].y-array[0].y};
        CanvasPoint p = {(int)round(array[0].x + t*v.x), (int)round(array[0].y+t*v.y)};
        return p;
    }

    CanvasPoint *newArray = new CanvasPoint[n-1];
    for (int i = 0; i < (n-1); i++)
    {
        Vector2i v = {array[i+1].x - array[i].x, array[i+1].y - array[i].y};
        CanvasPoint p = {(int)round(array[i].x + t*v.x), (int)round(array[i].y+t*v.y)};
        newArray[i] = p;
    }

    return pointBezhe(newArray, n-1, t);
}

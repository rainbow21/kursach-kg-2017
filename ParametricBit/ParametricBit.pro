#-------------------------------------------------
#
# Project created by QtCreator 2017-09-28T22:56:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ParametricBit
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
    mainwindow.cpp \
    log.cpp \
    Canvas/canvas.cpp \
    Canvas/drawgeom.cpp \
    cast.cpp \
    bezhe.cpp \
    geom/computing.cpp \
    scene/ivsceneobject.cpp \
    scene/ivscenestatic.cpp \
    scene/ivscenemovable.cpp \
    geom/edge.cpp \
    geom/plane.cpp \
    io/iosystem.cpp \
    io/func_for_string.cpp \
    io/func_for_file.cpp \
    scene/ivmodel.cpp \
    scene/ivcamera.cpp \
    double.cpp \
    geom/vector.cpp \
    geom/ivrotator.cpp \
    scene/scenepoint.cpp \
    color.cpp

HEADERS  += mainwindow.h \
    exception.h \
    log.h \
    geom/point.h \
    geom/edge.h \
    geom/vector.h \
    container/ivlist.h \
    container/ivnode.h \
    container/ivstack.h \
    Canvas/canvas.h \
    Canvas/drawgeom.h \
    Canvas/iv_math.h \
    Canvas/canvaspoint.h \
    cast.h \
    bezhe.h \
    color.h \
    geom/computing.h \
    content.h \
    geom/triangle.h \
    scene/ivscenestatic.h \
    scene/ivsceneobject.h \
    scene/ivscenemovable.h \
    scene/scenepoint.h \
    geom/plane.h \
    io/iosystem.h \
    define.h \
    io/func_for_file.h \
    io/func_for_string.h \
    scene/ivmodel.h \
    integer.h \
    geom/polygon.h \
    scene/ivcamera.h \
    double.h \
   geom/ivrotator.h \
    index.h \
    render/intersectinfo.h

FORMS    += mainwindow.ui

RESOURCES += \
    res.qrc

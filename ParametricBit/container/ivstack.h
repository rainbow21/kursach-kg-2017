#ifndef IVSTACK_H
#define IVSTACK_H
#include "ivnode.h"
typedef unsigned char byte;
/* === �������� ������� === */
// ���������� �����
#define ivStack(type, next)\
	ivStackBasis<type, (int)&(((type*)NULL)->next)>
// ����� ���������� �������� � �����
#define STACK_NEXT_PTR(element, type, offset)\
(*(type**)(((byte*)element) + offset))

template <class Type, int offset> class ivStackBasis
{
private:
	Type	*bottom;
	Type	*top;
	int		num;
	void	push(Type *element);	// ���������� �������� ��������������� �� ��� ������. ��������� ��� ������������� � ���������.
public:
			ivStackBasis();
			ivStackBasis(const ivStackBasis<Type, offset> &x);
			~ivStackBasis();
	void	push(Type element);
	Type*	pop();
	bool	isEmpty()	const;	
	int		getNum()	const;
};



template <class Type, int offset>
ivStackBasis<Type, offset>::ivStackBasis()
{
	bottom = top = NULL;
	num = 0;
}

template <class Type, int offset>
ivStackBasis<Type, offset>::~ivStackBasis()
{
	Type *cur = pop();
	while (cur)
	{
		delete cur;
		cur = pop();
	}
}

template <class Type, int offset> 
ivStackBasis<Type, offset>::ivStackBasis(const ivStackBasis<Type, offset> &x)
{
	bottom = top = NULL;
	num = 0;

	for (Type *i = x.top; i; i = i->next)
		push(*i);
}	

template <class Type, int offset>
void ivStackBasis<Type, offset>::push(Type *element)
{
	STACK_NEXT_PTR(element, Type, offset) = top;
	top = element;
	if (!bottom)
		bottom = element;
	num++;
}

template <class Type, int offset>
void ivStackBasis<Type, offset>::push(Type element)
{
	Type *newElement = new Type;
	*newElement = element;
	push(newElement);
}

template <class Type, int offset>
Type* ivStackBasis<Type, offset>::pop()
{
	Type *element;
	element = top;
	if (element)
	{
		top = STACK_NEXT_PTR(top, Type, offset);
		if (bottom == element)
			bottom = NULL;
		STACK_NEXT_PTR(element, Type, offset) = NULL;
		num--;
	}
	return element;
}

template <class Type, int offset>
bool ivStackBasis<Type, offset>::isEmpty() const
{
	return top ? false : true;
}

template <class Type, int offset>
int ivStackBasis<Type, offset>::getNum() const
{
	return num;
}

#define get() pop()
#define add(x) push(x)
#endif 


#ifndef IVNODE_H
#define IVNODE_H
/*
	���� � ����� ����� ����� ���������
	class <��� ����>
	{
	public:
		<���> value;
		<��� ����>* next;
	};
	����� <���> ������������� ����� �������� ��������
	��������� �� ��������� ������� ������ ����� ��� "next"
*/

// �������� ���� ��� �����. ��������� ������ �����. ���������� ��� ���� �� �����������.
#define ivNode(NameNode, Type)\
        class NameNode\
        {\
        public:\
            Type value;\
            NameNode* next;\
        }
#endif

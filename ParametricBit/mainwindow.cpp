#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QColorDialog>
#include <QFileDialog>
#include "io/iosystem.h"
#include "render/intersectinfo.h"

MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent), ui(new Ui::MainWindow)
{
    logLongTime(STD_LOG, "Create MainWindow");
    ui->setupUi(this);

    // Оформление
    this->setWindowIcon(QPixmap(":/img/resource/img/ic_3d_rotation_48.png"));


    // Создание камеры
    ScenePoint a = {100, -250, 250};
    ScenePoint b = {100, 250, 250};
    ScenePoint c = {100, 250, -250};
    ScenePoint d = {100, -250, -250};
    ScenePoint pivot = {1000, 0, 0};
    content.camera = new ivCamera(pivot, a,b,c,d,5);
    // Начальная инициализация Canvas
    ui->canvas->init();

    // Настройка action
    configAction();

    // Настройка таймера
    timer = new QTimer(this);
    timer->start(100);

    // Модели нет
    content.model = NULL;

    // Нажатие на кнопку button1
    connect(ui->button1, SIGNAL(clicked(bool)), this, SLOT(clickButton1()));
    // Нажатие на кнопку button2
    connect(ui->button2, SIGNAL(clicked(bool)), this, SLOT(clickButton2()));
    // Перемещение мыши в графическом окне
    connect(ui->canvas, SIGNAL(mouseMove(int,int)), this, SLOT(mouseMoveInCanvas(int,int)));
    // Нажатие мыши в графическом окне
    connect(ui->canvas, SIGNAL(mousePress(int,int)), this, SLOT(mousePressInCanvas(int,int)));
    // Нажатие на кнопку "Очистить точки"
    connect(ui->buttonClearListPoint, SIGNAL(clicked(bool)), this, SLOT(clickButtonClearListPoint()));
    // Нажатие на кнопку "Нарисовать ломанную"
    connect(ui->buttonPolyLine, SIGNAL(clicked(bool)), this, SLOT(clickButtonPolyLine()));
    // Нажатие на кнопку "Безье"
    connect(ui->buttonDrawBezhe, SIGNAL(clicked(bool)), this, SLOT(clickButtonDrawBezhe()));
    // Нажатие на кнопку "Треугольник"
    connect(ui->buttonTriangle, SIGNAL(clicked(bool)), this, SLOT(clickButtonDrawTriangle2()));
    // Нажатие на кнопку "File"
    connect(ui->buttonFile, SIGNAL(clicked(bool)), this, SLOT(clickButtonFile()));
    // Нажатие на кнопку "Триангуляция"
    connect(ui->buttonTriangulate, SIGNAL(clicked(bool)), this, SLOT(clickButtonTriangulate()));
    // Нажатие на кнопку "3D"
    connect(ui->button3D, SIGNAL(clicked(bool)), this, SLOT(clickButton3D()));
    // Нажатие на кнопки поворота камеры
    connect(ui->buttonRotateLeft, SIGNAL(clicked(bool)), this, SLOT(clickButtonRotateLeft()));
    connect(ui->buttonRotateRight, SIGNAL(clicked(bool)), this, SLOT(clickButtonRotateRight()));
    connect(ui->buttonRotateDown, SIGNAL(clicked(bool)), this, SLOT(clickButtonRotateDown()));
    connect(ui->buttonRotateUp, SIGNAL(clicked(bool)), this, SLOT(clickButtonRotateUp()));
    // Изменение полжения камеры
    connect(content.camera, SIGNAL(updateCoord(double,double,double)), this, SLOT(updateCamera(double,double,double)));

    // Истечение таймера отрисовки
    connect(timer, SIGNAL(timeout()), this, SLOT(timeOut()));
    // Нажатия на пункты меню
    connect(ui->actionClear, SIGNAL(triggered(bool)), this, SLOT(clickActionClear()));
    connect(ui->actionFillRed, SIGNAL(triggered(bool)), this, SLOT(clickActionFillRed()));
    connect(ui->actionFillGreen, SIGNAL(triggered(bool)), this, SLOT(clickActionFillGreen()));
    connect(ui->actionFillBlue, SIGNAL(triggered(bool)), this, SLOT(clickActionFillBlue()));
}

MainWindow::~MainWindow()
{

    logLongTime(STD_LOG, "Destroy MainWindow");
    delete content.camera;
    if (content.model) delete content.model;
    delete ui;
}

// ------------------------------------
// Реализация нажатия на кнопку button1
void MainWindow::clickButton1()
{
    try
    {
        logTime(STD_LOG, "entry \t void MainWindow::clickButton1()");
        throw(Ex_NullPtr("NullPtr in \"clickButton1\""));
        logTime(STD_LOG, "return \t void MainWindow::clickButton1()");
    }
    catch (UniversalException x)
    {
        QMessageBox::warning(this, "Warning", x.what());

        QString s("exception \t ");
        s.append(x.what());
        logTime(STD_LOG, s.toStdString().c_str());
        this->close();
    }
    catch (...)
    {
        QMessageBox::warning(this, "Warning", "Unknown error ....");

        QString s("exception \t Unkown error ...");
        logTime(STD_LOG, s.toStdString().c_str());
        this->close();
    }
}

// ------------------------------------
// Реализация нажатия на кнопку button2
void MainWindow::clickButton2()
{
    try
    {
        //QMessageBox::information(this, "inf", QString::number(IOSystem::searchString("in.txt", "fi", strlen("fi")+1)));
        FILE *f = fopen(STD_INPUT, "r");
        IOSystem::gotoString(f, 10);
        char buf[STD_STR_LEN];
        Read_string(f, buf, STD_STR_LEN);
        QMessageBox::information(this, "inf", buf);
        fclose(f);
    }
    catch (UniversalException e)
    {
        QMessageBox::warning(this, "Warning", e.what());
        QString s("esception \t ");
        s.append(e.what());
        logTime(STD_LOG, s.toStdString().c_str());
    }
    catch (...)
    {
        QMessageBox::warning(this, "Warning", "Unkown error ....");

        QString s("exception \t Unkown error ...");
        logTime(STD_LOG, s.toStdString().c_str());
    }
}

// ---------------------------------------------------------
// Реализация реакции на перемещение мыши в графическом окне
void MainWindow::mouseMoveInCanvas(int x, int y)
{
    this->ui->label_status->setText("<" + QString::number(x) + ", " + QString::number(y) + ">");
}

// -----------------------------------------------------
// Реализация реакции на нажатие мыши в графическом окне
void MainWindow::mousePressInCanvas(int x, int y)
{
    NodePoint2i point;
    point.value = {x, y};
    content.listPoint.append(point);

    // Сразу нарисовать линию
    clickButtonPolyLine();

    if (ui->cchBoxMarkPoint->isChecked())
        ui->canvas->markPoint(cast(point.value), ivcolor::white);
    else
        ui->canvas->drawPoint(cast(point.value), ivcolor::white);
}

// -----------------------------------------------------
// Реализация реакции на нажатие кнопки "Очистить точки"
void MainWindow::clickButtonClearListPoint()
{
    content.listPoint.clear();
    ui->canvas->clear();
}

// ----------------------------------------------------------
// Реализация реакции на нажатие кнопки "Нарисовать ломанную"
void MainWindow::clickButtonPolyLine()
{
    for (int i = 0; i < content.listPoint.getNum()-1; i++)
    {
        CanvasPoint p1 = cast(content.listPoint[i]->value);
        CanvasPoint p2 = cast(content.listPoint[i+1]->value);
        ui->canvas->drawLine(p1, p2, ivcolor::white);
    }
}

// --------------------------------------------
// Реализация реакции на нажатие кнопки "Безье"
void MainWindow::clickButtonDrawBezhe()
{
    try{

        int n = content.listPoint.getNum();
        if (n < 3) throw(Ex_FewPointBezhe("Few point for Bezhe in \"MainWindow::clickButtonDrawBezhe\""));
        ui->canvas->clear();
        for (int i = 0; i < (n-1); i++)
        {
            ui->canvas->markPoint(cast(content.listPoint[i]->value), ivcolor::white);
            ui->canvas->drawLine(cast(content.listPoint[i]->value), cast(content.listPoint[i+1]->value), ivcolor::white);
        }
        ui->canvas->markPoint(cast(content.listPoint[n-1]->value), ivcolor::white);

        CanvasPoint *ar = new CanvasPoint[n];
        for (int i = 0; i < n; i++)
            ar[i] = cast(content.listPoint[i]->value);
        ui->canvas->drawBezhe(ar, n, ivcolor::yellow);

        delete ar;
    }
    catch (UniversalException x)
    {
        QMessageBox::warning(this, "Error", x.what());
        QString s;
        s.append(x.what());
        logTime(STD_LOG, s.toStdString().c_str());
    }
    catch (...)
    {
        QMessageBox::warning(this, "Error", "Unkown error ...");
    }
}

// --------------------------------------------------
// Реализация реакции на нажатие кнопки "Треугольник"
void MainWindow::clickButtonDrawTriangle()
{
    try {
    int n = content.listPoint.getNum();
    if (n < 3) throw(Ex_FewPointTriangle("Few point for triangle in \"MainWindow::clickButtonDrawTriangle\""));
    ui->canvas->clear();
    ui->canvas->drawFillTriangle(cast(content.listPoint[n-1]->value),
                                cast(content.listPoint[n-2]->value),
                                cast(content.listPoint[n-3]->value),
                                ivcolor::yellow);
    ui->canvas->repaint();
    }
    catch (UniversalException x)
    {
        QMessageBox::warning(this, "Warning!", x.what());
        QString s;
        s.append(x.what());
        logTime(STD_LOG, s.toStdString().c_str());
    }
    catch (...)
    {
        QMessageBox::warning(this, "Warning!", "Unkown error ...");
    }
}

// ---------------------------------------------------------
// Альтернативная реализация на нажатие кнопки "Треугольник"
void MainWindow::clickButtonDrawTriangle2()
{
    ivList(NodeTriangle, next) list;

    Triangle<CanvasPoint> tr[2];
    NodeTriangle node;

    tr[0].p1 = {5, 5};
    tr[0].p2 = {20, 5};
    tr[0].p3 = {20, 20};
    node.value = tr[0];
    list.append(node);

    tr[1].p1 = {100, 100};
    tr[1].p2 = {100, 200};
    tr[1].p3 = {150, 150};
    node.value = tr[1];
    list.append(node);

    for (int i = 0; i < list.getNum(); i++)
        ui->canvas->drawFillTriangle(list[i]->value, QColor(200, 200, 200));

    ui->canvas->repaint();
}

// -------------------------------------------
// Реализация реакции на нажатие кнопки "File"
void MainWindow::clickButtonFile()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                QString::fromUtf8("Открыть файл"),
                                QDir::currentPath(),
                                "Model (*.txt);;All files (*.*)");

    printf("FILENAME: _%s_\n", filename.toStdString().c_str());

    if (strlen(filename.toStdString().c_str()))
    {
        this->content.model = new ivModel(filename.toStdString().c_str());
        Point3f center = content.model->getCenter();
        Vector3f v = {-center.x,-center.y, -center.z};
        content.model->moveOn(v);
        center = content.model->getCenter();
        QMessageBox::information(this, "center", QString::number(center.x) + ", " + QString::number(center.y) + ", " + QString::number(center.z));
        render3D();
    }
}

// -----------------------------------------
// Реализация реакции на нажатие кнопки "3D"
void MainWindow::clickButton3D()
{
    render3D();
}

// -----------------------------------------------------
// Реализация реакции на нажатие кнопки "поворот вправо"
void MainWindow::clickButtonRotateRight()
{
    content.camera->rotate(0.0,0.0,15.0);
    render3D2();
}

// ----------------------------------------------------
// Реализация реакции на нажатие кнопки "поворот влево"
void MainWindow::clickButtonRotateLeft()
{
    content.camera->rotate(0.0,0.0,-15.0);
    render3D2();
}

// ----------------------------------------------------
// Реализация реакции на нажатие кнопки "поворот вверх"
void MainWindow::clickButtonRotateUp()
{
    content.camera->rotate(0.0, -15.0,0.0);
    render3D2();
}

// ----------------------------------------------------
// Реализация реакции на нажатие кнопки "поворот вниз"
void MainWindow::clickButtonRotateDown()
{
    content.camera->rotate(0.0, 15.0,0.0);
    render3D2();
}

// ---------------------------
// Обновление положения камеры
void MainWindow::updateCamera(double x, double y, double z)
{
    QString s("Камера: " + QString::number(x, (char)3) + ", " + QString::number(y, (char)3) + ", " + QString::number(z, (char)3));
    ui->labelCamera->setText(s);
}

// ---------------------------------------------------
// Реализация реакции на нажатие кнопки "Триангуляция"
void MainWindow::clickButtonTriangulate()
{
    srand(time(NULL));
    ivList(NodeTriangle, next) triangls = triangleInterPoint(content.listPoint);
    QColor colors[5] = {ivcolor::white, ivcolor::red, ivcolor::green, ivcolor::blue, ivcolor::yellow};
    for (int i = 0; i < triangls.getNum(); i++)
    {
        int ind = rand()%5;
        Triangle<CanvasPoint> tr = triangls[i]->value;
        ui->canvas->drawFillTriangle(tr.p1, tr.p2, tr.p3, colors[ind]);
    }
}

// --------------------------------------
// Реакция на истечение таймера отрисовки
void MainWindow::timeOut()
{
    //ui->canvas->repaint();
}


/* подготовить домены
 * зарегаться
 *
 * лист под почту
 * лист под данные
*/

// ----------------------
// Насройка action в меню
void MainWindow::configAction()
{
    ui->actionClear->setShortcut(QKeySequence::fromString("Ctrl+C"));
    ui->actionFillRed->setShortcut(QKeySequence::fromString("Ctrl+R"));
    ui->actionFillGreen->setShortcut(QKeySequence::fromString("Ctrl+G"));
    ui->actionFillBlue->setShortcut(QKeySequence::fromString("Ctrl+B"));
}

// -------------
// Прорисовка 3D
void MainWindow::render3D()
{
    ui->canvas->clear();
    //ivModel model(STD_INPUT);
    ScenePoint p = content.camera->getPivot();
    // Перебрать все точки. Построить лучи до камеры. И найти пересечения с картинной плоскостью.
    for (int i = 0; i<content.model->getNumPoint(); i++)
    {

        ScenePoint pi = content.model->getPoint(i);
        Vector3f v = {pi.x - p.x, pi.y - p.y, pi.z - p.z}; // Вектор (луч) из точки camera.pivot
        Plane plane = content.camera->getImagePlane();

        double t = -(double)(plane.getD() + plane.getA()*p.x + plane.getB()*p.y + plane.getC()*p.z) / \
                    (double)(plane.getA()*v.x + plane.getB()*v.y + plane.getC()*v.z);
        // Находим точку пересечения
        ScenePoint intersect = {p.x + t*v.x, p.y+t*v.y, p.z+t*v.z};
        //QMessageBox::information(this, "", QString::number(intersect.x) + ", " + QString::number(intersect.y) +  ", " + QString::number(intersect.z));
        // Теперь надо узнать. Лежит ли он внутри прямоугольника
        if (content.camera->pointIn(intersect))
        {
            // Найти координаты на экране
            Vector3f vx = {content.camera->p1.x-content.camera->p2.x,\
                          content.camera->p1.y - content.camera->p2.y,\
                          content.camera->p1.z - content.camera->p2.z};
            Vector3f vy = {content.camera->p4.x - content.camera->p1.z,\
                           content.camera->p4.y - content.camera->p1.y,\
                           content.camera->p4.z - content.camera->p1.z};
            Vector3f vp = {intersect.x - content.camera->p1.x,\
                           intersect.y - content.camera->p1.y,\
                           intersect.z - content.camera->p1.z};
            // Проекция на vx
            double cosX = getCos(vx, vp);
            double prX = fabs(lenght(vp)*cosX);

            // Проекция на vy
            double cosY = getCos(vy, vp);
            double prY = fabs(lenght(vp)*cosY);
            CanvasPoint p = {(int)prX, (int)prY};
            ui->canvas->markPoint(p, QColor(0,255,0));
        }
    }

    ui->canvas->repaint();
}

void MainWindow::render3D2()
{
    ui->canvas->clear();
    if (content.model)
    {
        // Вычислить координаты точки на экране в пространстве
        for (int i = 0; i < content.camera->getHeight(); i++)
            for (int j = 0; j < content.camera->getWidth(); j++)
            {
                ScenePoint pix3D = content.camera->getPixelCoord(i,j);
                ScenePoint pivot = content.camera->getPivot();
                Vector3f v = {pix3D.x - pivot.x, pix3D.y - pivot.y, pix3D.z - pivot.z};
                CanvasPoint pixel = {j, i};
                // Прямая по вектору v из опорной точки камеры
                InterInfo info = content.model->intersect(pivot, v);
                if (info.intersect)
                    ui->canvas->drawPoint(pixel, info.color);
            }
    }
    ui->canvas->repaint();
}

void MainWindow::clickActionClear()
{
    ui->canvas->clear();
}

void MainWindow::clickActionFillRed()
{
    ui->canvas->fill(ivcolor::red);
}

void MainWindow::clickActionFillGreen()
{
    ui->canvas->fill(ivcolor::green);
}

void MainWindow::clickActionFillBlue()
{
    ui->canvas->fill(ivcolor::blue);
}

// Обработка нажатия на клавиатуру
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    Vector3f v = {0,0,0};
    ScenePoint point = content.camera->pivotPoint;
    ScenePoint start = {0,0,0};


    if (event->key() == Qt::Key_W)
    {
        double d = distance(point, start);
        if (d <= 210)
            return;

        v = content.camera->getImagePlane().getNormal();

        ScenePoint p1 = content.camera->p1;
        p1.x += v.x;
        p1.y += v.y;
        p1.z += v.z;
        ScenePoint p2 = content.camera->p1;
        p2.x -= v.x;
        p2.y -= v.y;
        p2.z -= v.z;
        // Узнать какая точка ближе к 0,0,0
        double d1 = distance(p1, start);
        double d2 = distance(p2, start);
        if (d2 < d1)
        {
            v.x *= -1;
            v.y *= -1;
            v.z *= -1;
        }

        v.x /= lenght(v);
        v.y /= lenght(v);
        v.z /= lenght(v);

        v.x *= 5;
        v.y *= 5;
        v.z *= 5;

    }
    if (event->key() == Qt::Key_S)
    {
        v = content.camera->getImagePlane().getNormal();

        ScenePoint p1 = content.camera->p1;
        p1.x += v.x;
        p1.y += v.y;
        p1.z += v.z;
        ScenePoint p2 = content.camera->p1;
        p2.x -= v.x;
        p2.y -= v.y;
        p2.z -= v.z;
        // Узнать какая точка ближе к 0,0,0
        double d1 = distance(p1, start);
        double d2 = distance(p2, start);
        if (d2 > d1)
        {
            v.x *= -1;
            v.y *= -1;
            v.z *= -1;
        }

        v.x /= lenght(v);
        v.y /= lenght(v);
        v.z /= lenght(v);

        v.x *= 5;
        v.y *= 5;
        v.z *= 5;
    }
    if (event->key() == Qt::Key_A)
    {
        ScenePoint center = {0,0,0};
        center.x += content.camera->p1.x + content.camera->p2.x + content.camera->p3.x + content.camera->p4.x;
        center.y += content.camera->p1.y + content.camera->p2.y + content.camera->p3.y + content.camera->p4.y;
        center.z += content.camera->p1.z + content.camera->p2.z + content.camera->p3.z + content.camera->p4.z;
        center.x /= 4;
        center.y /= 4;
        center.z /= 4;
        ScenePoint p1 = content.camera->p1;
        ScenePoint p4 = content.camera->p4;
        Vector3f v41 = {p1.x-p4.x, p1.y-p4.y, p1.z-p4.z};
        v41.x /= 2.0;
        v41.y /= 2.0;
        v41.z /= 2.0;

        ScenePoint Pv = {p4.x + v41.x, p4.y + v41.y, p4.z + v41.z};

        v = {Pv.x-center.x, Pv.y-center.y, Pv.z-center.z};
        normalize(v);

        multi(v, 5.0);
    }
    if (event->key() == Qt::Key_D)
    {
        ScenePoint center = {0,0,0};
        center.x += content.camera->p1.x + content.camera->p2.x + content.camera->p3.x + content.camera->p4.x;
        center.y += content.camera->p1.y + content.camera->p2.y + content.camera->p3.y + content.camera->p4.y;
        center.z += content.camera->p1.z + content.camera->p2.z + content.camera->p3.z + content.camera->p4.z;
        center.x /= 4;
        center.y /= 4;
        center.z /= 4;
        ScenePoint p1 = content.camera->p1;
        ScenePoint p4 = content.camera->p4;
        Vector3f v41 = {p1.x-p4.x, p1.y-p4.y, p1.z-p4.z};
        v41.x /= 2.0;
        v41.y /= 2.0;
        v41.z /= 2.0;

        ScenePoint Pv = {p4.x + v41.x, p4.y + v41.y, p4.z + v41.z};

        v = {Pv.x-center.x, Pv.y-center.y, Pv.z-center.z};
        normalize(v);

        multi(v, -5.0);
    }

    content.camera->moveOn(v);
    render3D2();

}

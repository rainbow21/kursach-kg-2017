#ifndef DEFCOLOR_H
#define DEFCOLOR_H
#include <QColor>
#include <string>
namespace ivcolor
{
const QColor white = QColor::fromRgb(255,255,255);
const QColor black = QColor::fromRgb(0,0,0);
const QColor red = QColor::fromRgb(255,0,0);
const QColor green = QColor::fromRgb(0,255,0);
const QColor blue = QColor::fromRgb(0,0,255);
const QColor yellow = QColor::fromRgb(0xFF, 0xEB, 0x3B);
// Коды поддерживаемых цветов
#define WHITE   0
#define BLACK   1
#define RED     2
#define GREEN   3
#define BLUE    4
#define YELLOW  5
}

QColor getEnableColor(int i);

#endif // COLOR_H

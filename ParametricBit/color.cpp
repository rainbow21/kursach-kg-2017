#include "color.h"

QColor getEnableColor(int i)
{
    switch (i) {
    case WHITE:
        return ivcolor::white;
    case BLACK:
        return ivcolor::black;
    case RED:
        return ivcolor::red;
    case GREEN:
        return ivcolor::green;
    case BLUE:
        return ivcolor::blue;
    case YELLOW:
        return ivcolor::yellow;
    default:
        return ivcolor::white;
    }
}

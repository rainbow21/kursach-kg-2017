#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "exception.h"
#include "log.h"
#include <time.h>
#include "content.h"
#include <Canvas/canvas.h>
#include <QTimer>
#include <cast.h>
#include <color.h>
#include <geom/computing.h>
#include "scene/ivmodel.h"
#include "geom/plane.h"
#include <QKeyEvent>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void configAction();
    void render3D();
    void render3D2();
    virtual void keyPressEvent(QKeyEvent *event);

private:
    Ui::MainWindow *ui;
    Content content;
    QTimer *timer;

private slots:
    void clickButton1();
    void clickButton2();
    void mouseMoveInCanvas(int x, int y);
    void mousePressInCanvas(int x, int y);
    void clickButtonClearListPoint();
    void clickButtonPolyLine();
    void clickButtonDrawBezhe();
    void clickButtonDrawTriangle();     // Рисует заполненный треугольник
    void clickButtonDrawTriangle2();    // Рисует набо треугольников
    void clickButtonTriangulate();
    void clickButtonFile();
    void clickButton3D();
    void clickButtonRotateRight();
    void clickButtonRotateLeft();
    void clickButtonRotateUp();
    void clickButtonRotateDown();
    void updateCamera(double, double, double);

    void timeOut();
    // action
    void clickActionFillRed();
    void clickActionFillGreen();
    void clickActionFillBlue();
    void clickActionClear();

};

#endif // MAINWINDOW_H

#ifndef IOSYSTEM_H
#define IOSYSTEM_H

/*  Система ввода/вывода
 *
 *
 *
 *
 *
 *
*/
#include <stdio.h>
#include "exception.h"
#include <string.h>
#include "define.h"
#include "io/func_for_file.h"
#include "io/func_for_string.h"


class ivModel;

class IOSystem
{
public:
    static int      searchString(char *filename, char *str, int n)  throw(Ex_FileNotFound);  // Номер строки, в котором находится искомое выражение
    static void     gotoString(FILE *f, int  k);                              // Перейти к выбранной стрке в файле (открыт)
    static char*    readString(FILE *f);
    static void     getNextString(FILE *f, char *obraz, int n);
    static void     write(FILE *f, char *str);
};

#endif // IOSYSTEM_H

#ifndef FOR_FILE
#define FOR_FILE
#include "stdio.h"
// Считывание из файла переменной K (количества тегов)
int Read_K(FILE *f);

// Считывание специальных тегов
void Read_separator(FILE *f,char **sep, int K);

// Считывание строки
void Read_string(FILE *f, char *buf, int n);

// Считывание количества строк (N)
int Read_N(FILE *f);

// Считыание количества запросов M
int Read_M(FILE *f);

#endif

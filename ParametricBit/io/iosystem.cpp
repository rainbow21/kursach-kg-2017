#include "iosystem.h"
#include <QMessageBox>
int IOSystem::searchString(char *filename, char *str, int n) throw(Ex_FileNotFound)
{
    FILE *f = fopen(filename, "r");
    if (!f)
        throw(Ex_FileNotFound("Error: File not found in \"IOSystem::searchString\" "));


    char buf[STD_STR_LEN];
    Full(buf, STD_STR_LEN, '\0');
    int result = -1;
    int i = 0;
    while (Compare_String(buf, (char*)"END", 3))
    {
        Full(buf, STD_STR_LEN, '\0');
        Read_string(f, buf, STD_STR_LEN);
        if (!Compare_String(buf, str, n))
        {
            result = i;
            break;
        }
        i++;
    }
    fclose(f);

    return result;
}

void IOSystem::gotoString(FILE *f, int k)
{
    char buf[128];
    for (int i = 0; i < k; i++)
        Read_string(f, buf, 128);
}


char* IOSystem::readString(FILE *f)
{
    char *buf = new char[STD_STR_LEN];
    Full(buf, STD_STR_LEN, '\0');
    Read_string(f, buf, STD_STR_LEN);
    return buf;
}


void IOSystem::getNextString(FILE *f, char *obraz, int n)
{
    int i;
    for (i = 0; f->_ptr[i] != '\n' && i < n;i++)
        obraz[i] = f->_ptr[i];
    obraz[i] = '\0';
}

// -------------------------------
// Написать строку в открытый файл
void IOSystem::write(FILE *f, char *str)
{
    fprintf(f, "%s", str);
}

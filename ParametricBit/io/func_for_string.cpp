#include <stdlib.h>
#include <stdio.h>
#include "func_for_string.h"

void Lower(char* x, int n)
{
    int i = 0;

    while (x[i] != '\0' && i < n)
    {
        if ((int)x[i] >= 65 && (int)x[i] <=90)
        {
            char c = (char)( (int)x[i] + 32);
            x[i] = c;
        }
        i++;
    }
}

void Full(char *m, int n, char x)
{
    for (int i = 0; i < n; i++)
        m[i] = x;
    m[n-1] = '\0';
}

int Compare_String(char *a, char *b, int n)
{
    int r = 0;
    for (int i = 0; i < n; i++)
    {
        if (a[i] != b[i])
            return a[i] - b[i];
    }
    return r;
}

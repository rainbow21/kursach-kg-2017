#include <stdlib.h>
#include <stdio.h>
#include "func_for_string.h"
#include "func_for_file.h"

int Read_K(FILE *f)
{
    int r;
    char c;
    fscanf(f,"%d",&r);
    fscanf(f,"%c",&c); // '\n'

    return r;
}

void Read_separator(FILE *f, char **sep, int K)
{
    for (int i = 0; i < K;i++)
        Read_string(f, sep[i],251);
}

void Read_string(FILE *f, char *buf, int n)
{
    int i = 0;
    char c = 'p';
    int er = 0;
    while (c != '\n' && i < n)
    {
        er = fscanf(f,"%c",&c);
        if (er == -1)
            break;
        if (c != '\n')
            buf[i++] = c;
    }

    buf[i] = '\0';
}

int Read_N(FILE *f)
{
    int r;
    char c;
    fscanf(f,"%d",&r);
    fscanf(f,"%c",&c); // '\n'

    return r;
}

int Read_M(FILE *f)
{
    int r;
    char c;
    fscanf(f,"%d",&r);
    fscanf(f,"%c",&c);

    return r;
}

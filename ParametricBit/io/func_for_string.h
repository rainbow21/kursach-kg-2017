#ifndef FOR_STRING
#define FOR_STRING
// Приведение строки к нижнему регистру
/// Получает строку и ее длину
void Lower(char* x, int n);

/// Заполняет строку указанными символом.
void Full(char *m, int n, char x);

/// Сравнивает строки до определенного символа
int Compare_String(char *a, char *b, int n);


#endif // FUNC_H

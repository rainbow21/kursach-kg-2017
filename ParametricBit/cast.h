#ifndef CAST_H
#define CAST_H

#include <Canvas/canvaspoint.h>
#include <geom/point.h>
#include <scene/scenepoint.h>

Point2i         cast(const CanvasPoint  &x2);
CanvasPoint     cast(const Point2i      &x2);

////Point3f         cast(const ScenePoint   &p);
//ScenePoint      cast(const Point3f      &p);

ScenePoint      cast(const Point3i  &p);

#endif // CAST_H

#ifndef POINT_H
#define POINT_H

#include "container/ivnode.h"
#include <math.h>

/*-----------*/
// Плоскость //
/*-----------*/

// Точка на плоскости с целочисленными координатами
struct Point2i
{
    int x, y;
};
typedef struct Point2i Point2i;

// Точка на плоскости с целочисленными координатами
struct Point2f
{
    float x, y;
};
typedef struct Point2f Point2f;

/*--------------*/
// Пространство //
/*--------------*/

// Точка в пространстве с целочисленными координатами
struct Point3i
{
    int x,y,z;
};
typedef struct Point3i Point3i;

// Точка в пространстве с вещественными координатами
struct Point3f
{
    double x,y,z;
};
typedef struct Point3f Point3f;

typedef Point2f Point2; // По умолчанию у точки на плоскости координаты вещественные
typedef Point3f Point3; // По умолчанию у точки в пространстве координаты вещественные
typedef Point3f Point;  // В общем случае используется трехмерная точка с вещественными координатами

ivNode(NodePoint2i, Point2i);
ivNode(NodePoint3f, Point3f);

//double distance(Point3f p1, Point3f p2);

#endif // POINT2D_H

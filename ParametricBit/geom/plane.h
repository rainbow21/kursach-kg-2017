#ifndef PLAIN_H
#define PLAIN_H

#include "geom/vector.h"
#include "geom/point.h"
#include "geom/edge.h"
#include "container/ivlist.h"
#include "scene/scenepoint.h"
#include "cast.h"

// Обертка для списка ребер
ivNode(NodePoint3f_2, Point3f);

class Plane
{
private:
    double A,B,C,D;
public:
    Plane(Point3f p1, Point3f p2, Point3f p3);
    Plane(double a, double b, double c, double d);
    Vector3f getNormal();
    Vector3f getNormal(Point3f p); // Нормаль в сторону заданной точки
    static Plane getPlane(ivList(NodeEdge3f, next) list);
    static Plane getPlane(ivList(NodePoint3f, next) list);
    static Plane getPlane(ivList(NodeScenePoint, next) list);
    double getA() const;
    double getB() const;
    double getC() const;
    double getD() const;
    ScenePoint getIntersect(ScenePoint p, Vector3f v);
    /// Проверка на принадлежность плоскости
    bool    exists(ScenePoint p);
};

// Обертка над гранями (плоскостями) для списков
ivNode(NodePlane, Plane);



#endif // PLAIN_H


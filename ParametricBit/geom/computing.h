#ifndef COMPUTING_H
#define COMPUTING_H

#include <Canvas/canvaspoint.h>
#include <container/ivlist.h>
#include <container/ivstack.h>
#include <geom/triangle.h>
#include <exception.h>
#include <math.h>
#include <geom/vector.h>
#include <cast.h>
#include <stdio.h>
#include <geom/edge.h>

/*  В данном файле будет осуществляться метматический расчет, необходимый в геометрии.
 *
*/
// Триангуляция методом "вторгающихся вершин"
ivList(NodeTriangle, next) triangleInterPoint(ivList(NodePoint2i, next) list);

// Псевдоскалярное произведение [ABxBC]
double psevdoScalarMul(Point2i A, Point2i B, Point2i C);

// Векторное произведение
double vectorMul(int ax, int ay, int bx, int by);

// Пересекаются ли 2 отрезка?
bool crossLines(Edge2i l1, Edge2i l2);

#endif // COMPUTING_H

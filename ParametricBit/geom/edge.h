#ifndef EDGE_H
#define EDGE_H
#include "geom/point.h"

/*-------------------*/
// Плоскость - грань //
/*-------------------*/

// Отрезок на плоскости с целочисленными координатами
struct Edge2i
{
    Point2i v1, v2;
};
typedef struct Edge2i Edge2i;

// Отрезок на плоскости с вещественными координатами
struct Edge2f
{
    Point2f v1, v2;
};
typedef struct Edge2f Edge2f;

/*--------------*/
// Пространство //
/*--------------*/

// Отрезок в пространстве с целочисленными координатами
struct Edge3i
{
    Point3i v1, v2;
};
typedef struct Edge3i Edge3i;

// Отрезок в пространстве с вещественными координатами
struct Edge3f
{
    Point3f v1, v2;
};
typedef struct Edge3f Edge3f;

typedef Edge2f Edge2;
typedef Edge3f Edge3;
typedef Edge3f Edge;

// Обертка над гранями для списков
ivNode(NodeEdge3f, Edge3f);
#endif // EDGE_H

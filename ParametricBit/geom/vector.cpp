#include "vector.h"

// Узнать cos между двумя векторами
double getCos(Vector3f v1, Vector3f v2)
{
    return (double)(v1.x*v2.x + v1.y*v2.y + v1.z*v2.z)/(double)(lenght(v1)*lenght(v2));
}

// Узнать длину вектора
double lenght(Vector3f v)
{
    return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

// Нормализация ветора
void normalize(Vector3f &v)
{
    double l = lenght(v);
    v.x /= l;
    v.y /= l;
    v.z /= l;
}

// Умножение вектора на число
void multi(Vector3f &v, double k)
{
    v.x *= k;
    v.y *= k;
    v.z *= k;
}


// Векторное произведение
Vector3f multiVec(Vector3f a, Vector3f b)
{
    Vector3f r = {
        a.y*b.z - a.z*b.y,
        a.z*b.x - a.x*b.z,
        a.x*b.y - a.y*b.x
    };
    return r;
}

// Вектора параллельны
bool    isParallel(Vector3f a, Vector3f b)
{
    double cosAB = getCos(a,b);
    return eq(cosAB, 1) || eq(cosAB, -1);
}

// Вектора перпендикулярны
bool    isOrto(Vector3f a, Vector3f b)
{
    return eq(getCos(a,b), 0);
}

// Расстояние от точки до прямой
double distance(Point3f p, Point3f pivot, Vector3f v)
{
    Vector3f m = {pivot.x-p.x, pivot.y-p.y, pivot.z-p.z};
    Vector3f rVec = multiVec(m, v);
    return lenght(rVec) / lenght(v);
}


// Создать вектор на основе двух точек
Vector3f createVector3f(Point3f p1, Point3f p2)
{
    Vector3f v = {p2.x-p1.x, p2.y-p1.y, p2.z-p1.z};
    return v;
}

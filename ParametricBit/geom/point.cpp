#include "geom/point.h"
#include "geom/vector.h"

double distance(Point3f p1, Point3f p2)
{
    Vector3f v = {p2.x - p1.x, p2.y - p1.y, p2.z - p1.z};
    return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

#include "geom/plane.h"
#include "define.h"
#include <stdio.h>
// Default Constructor
double Plane::getA() const
{
    return A;
}

double Plane::getB() const
{
    return B;
}

double Plane::getC() const
{
    return C;
}

double Plane::getD() const
{
    return D;
}

Plane::Plane(Point3f p1, Point3f p2, Point3f p3)
{
    // Vector
    Vector3f a = {p1.x - p2.x, p1.y - p2.y, p1.z - p2.z};
    Vector3f b = {p3.x - p2.x, p3.y - p2.y, p3.z - p2.z};
    Vector3f n = {
        a.y*b.z - a.z*b.y,
        a.z*b.x - a.x*b.z,
        a.x*b.y - a.y*b.x
    };
    normalize(n);

    this->A = n.x;
    this->B = n.y;
    this->C = n.z;
    // Подставить в уравнение плоскости и получить D
    this->D = -(A*p1.x + B*p1.y + C*p1.z);
}

Plane::Plane(double a, double b, double c, double d)
{
    this->A = a;
    this->B = b;
    this->C = c;
    this->D = d;
}

Vector3f Plane::getNormal()
{
    Vector3f v = {A, B, C};
    return v;
}

Vector3f Plane::getNormal(Point3f p) // Нормаль в сторону заданной точки
{
    Vector3f n1 = getNormal();
    Vector3f n2 = {-n1.x, -n1.y, -n1.z};
    Point3f pres;
    if (A)
        pres = {-D/A,0, 0};
    else if (B)
        pres = {0, -D/B, 0};
        else if (C)
            pres = {0, 0, -D/C};
            else
            {
                // Какой-то частный случай
                Vector3f v = {0,0,0};
                return v;
            }
    Point3f p1 = {pres.x + n1.x, pres.y + n1.y, pres.z + n1.z};
    Point3f p2 = {pres.x + n2.x, pres.y + n2.y, pres.z + n2.z};
    if (distance(p1, p) < distance(p2, p))
        return n1;
    else
        return n2;

}

Plane Plane::getPlane(ivList(NodeEdge3f, next) list)
{
    Edge3f edg1 = list[0]->value;
    Edge3f edg2 = list[1]->value;
    Plane p(edg1.v1, edg1.v2, edg2.v2);
    return p;
}

Plane Plane::getPlane(ivList(NodePoint3f, next) list)
{
    Plane p(list[0]->value, list[1]->value, list[2]->value);
    return p;
}

Plane Plane::getPlane(ivList(NodeScenePoint, next) list)
{
    Plane p(list[0]->value, list[1]->value, list[2]->value);
    return p;
}


ScenePoint Plane::getIntersect(ScenePoint p, Vector3f v)
{
    // Точка пересечения прямой и плоскости
    // Параметр t
    // Считал на листочке, там не сложно
    double t = (-1) * (double)(A*p.x + B*p.y + C*p.z + D)/(double)(A*v.x + B*v.y + C*v.z);
    ScenePoint r = {p.x, p.y, p.z};
    r.x += t*v.x;
    r.y += t*v.y;
    r.z += t*v.z;
    return r;
}

bool Plane::exists(ScenePoint p)
{
    return eq(A*p.x + B*p.y + C*p.z + D, 0);
}

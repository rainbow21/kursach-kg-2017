#include <geom/computing.h>


// Метод вторгающихся вершин. Полигон не должен быть самопересекающимся!
/* Отсечь треугольник, внутри которй не будет других вершин.
 * Ищем вершину, внутренни угол при которой меньше 180.
 * Берем левую и правую соседние вершины. (обход по часовой стрелки)
 * Пытаемся их соеденить. Помещаем его в список готовых треугольников.
 * Удаляем исходную (центральную вершину) и продолжаем работать.
 * Если появляется вторгающаяся вершина, то ...
 *  1) Разбить полигон, который мы еще не треангулировали. От одной из соседней вершины до вторгшейся точки.
 * Рекурсивно дял получившихся ттреугольников
*/
// ------------------------------------------
// Триангуляция методом "вторгающихся вершин"
ivList(NodeTriangle, next) triangleInterPoint(ivList(NodePoint2i, next) list)
{
    ivList(NodeTriangle, next) resultList;
    int n = list.getNum();
    if (n == 3)
    {
        printf("n == 3;\n");
        Triangle<CanvasPoint> tr;
        tr.p1 = cast(list[0]->value);
        tr.p2 = cast(list[1]->value);
        tr.p3 = cast(list[2]->value);
        NodeTriangle nd = {tr, NULL};
        resultList.append(nd);
        return resultList;
    }


    // true - "+", false - "-"
    bool *scalarMul = new bool[n];
    for (int i = 0; i < n; i++)
        scalarMul[i] = false;
    for (int i = 1; i < (n-1); i++)
        scalarMul[i] = psevdoScalarMul(list[i-1]->value, list[i]->value, list[i+1]->value) > 0 ? true : false;
    // Для начальной трйоки
    scalarMul[0] = psevdoScalarMul(list[n-1]->value, list[0]->value, list[1]->value) > 0 ? true : false;
    // Для конечной тройки
    scalarMul[n-1] = psevdoScalarMul(list[n-2]->value, list[n-1]->value, list[0]->value) > 0 ? true : false;

    // Есть ли входящие?
    bool flag = false;
    for (int i = 1; i < n; i++)
        if (scalarMul[i-1] != scalarMul[i])
        {
            flag = true;
            break;
        }
    // Просмотр крайних
    if (scalarMul[n-1] != scalarMul[0])
        flag = true;
    if (!flag)
    {
        printf("Vipuklii\n");
        // Многоугольник выпуклый. Делим ребрами, исходящими из 0-ой вершины
        for (int i = 1; i < (n-1); i++)
        {
            NodeTriangle nd;
            nd.value.p1 = cast(list[i]->value);
            nd.value.p2 = cast(list[i+1]->value);
            nd.value.p3 = cast(list[0]->value);
            resultList.append(nd);
        }
        return resultList;
    }
    else
    {
        printf("Vognutii\n");
        // Многоугольник вогнутый
        // Узнать каких вершин больше?
        int count_true = 0;
        int count_false = 0;

        for (int i = 0; i < n; i++)
            scalarMul[i] ? count_true++ : count_false++;
        // Тив входящих точек
        bool intro_point;
        intro_point = count_true > count_false ? false : true;
        // Ищем первую же входящую точку
        int ind_intro;
        for (ind_intro = 0; ind_intro < n; ind_intro++)
            if (scalarMul[ind_intro] == intro_point)
                break;

        // Находим диагональ из этой точки, которая полностью лежит в многоугольнике
        Edge2i diag;
        int indl, indr; // Индексы правых и левых соседей (точек)
        if (ind_intro != 0)
            indl = ind_intro - 1;
        else
            indl = n-1;

        if (ind_intro != n-1)
            indr = ind_intro + 1;
        else
            indr = 0;

        int i1 = 0, i2 = 0;

        for (int i = 0; i < n; i++)
        {
            if (i == ind_intro || i == indr || i == indl)
                continue;

            bool find = false;  // Найдена ли "внутренняя" диагональ
            diag = {list[ind_intro]->value, list[i]->value};
            i1 = ind_intro;
            i2 = i;
            Edge2i gr;
            for (int j = 1; j < n; j++)
            {
                if (j == i || j-1 == i)
                    continue;
                gr = {list[j-1]->value, list[j]->value};
                if (!crossLines(gr, diag))
                    find = true;
            }
            // Проверка крайней пары
            gr = {list[n-1]->value, list[0]->value};
            if (!crossLines(gr, diag))
                find = true;

            if (find)
                break;
        }

        // i1 - должно быть всегда меньше
        if (i1 > i2)
        {
            int ip = i1;
            i1 = i2;
            i2 = ip;
        }

        // i1,i2 образуют рассекающую грань
        ivList(NodePoint2i, next) pol1;
        ivList(NodePoint2i, next) pol2;
        for (int i = 0; i < n; i++)
        {
            if (i <= i1 ||  i2 <= i)
            {
                NodePoint2i nd = {list[i]->value, NULL};
                pol1.append(nd);
            }
            if (i1 <= i && i <= i2)
            {
                NodePoint2i nd = {list[i]->value, NULL};
                pol2.append(nd);
            }
        }
        ivList(NodeTriangle, next) trList1 = triangleInterPoint(pol1);
        ivList(NodeTriangle, next) trList2 = triangleInterPoint(pol2);
        for (int i = 0; i < trList1.getNum(); i++)
            resultList.append(*trList1[i]);
        for (int i = 0; i < trList2.getNum(); i++)
            resultList.append(*trList2[i]);
        return resultList;

    }



    // Найти вершину, с острым внутренним углом углом
    return resultList;
}


// Псевдоскалярное произведение [ABxBC]
double psevdoScalarMul(Point2i A, Point2i B, Point2i C)
{
    return (B.x-A.x)*(C.y-B.y) - (B.y-A.y)*(C.x-B.x);
}

// Векторное произведение
double vectorMul(int ax, int ay, int bx, int by)
{
    return ax*by-ay*bx;
}

// Пересекаются ли 2 отрезка?
bool crossLines(Edge2i l1, Edge2i l2)
{
    Point2i p1 = l1.v1;
    Point2i p2 = l1.v2;
    Point2i p3 = l2.v1;
    Point2i p4 = l2.v2;

    double v1 = vectorMul(p4.x-p3.x,p4.y-p3.y,p1.x-p3.x,p1.y-p3.y);
    double v2 = vectorMul(p4.x-p3.x,p4.y-p3.y,p2.x-p3.x,p2.y-p3.y);
    double v3 = vectorMul(p2.x-p1.x,p2.y-p1.y,p3.x-p1.x,p3.y-p1.y);
    double v4 = vectorMul(p2.x-p1.x,p2.y-p1.y,p4.x-p1.x,p4.y-p1.y);

    if (v1*v2 < 0 && v3*v4 < 0)
        return true;
    else
        return false;
}

#ifndef POLYGON_H
#define POLYGON_H

#include "scene/scenepoint.h"
#include "container/ivlist.h"

// Полигоны 3Д => грань
struct Polygon3f
{
    ivList(NodeScenePoint, next) point;
};

// Обертка над гранью. Для списка
ivNode(NodePolygon3f, Polygon3f);

#endif // POLYGON_H

#ifndef IVROTATOR_H
#define IVROTATOR_H
#include "scene/scenepoint.h"
#include "geom/point.h"
#include <math.h>
#include "geom/vector.h"

class ivRotator
{
public:
    // Вращение в различных плоскостях
    static void Rotate2D_XY(ScenePoint &P, double alpha);
    static void Rotate2D_XZ(ScenePoint &P, double alpha);
    static void Rotate2D_YZ(ScenePoint &P, double alpha);

    // Поврот 3D точки относительно оси
    static void rotate3D(ScenePoint &P, Vector3f V);
};

#endif // IVROTATOR_H

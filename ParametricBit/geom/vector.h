#ifndef VECTOR_H
#define VECTOR_H
#include "point.h"
#include <math.h>
#include "double.h"

typedef Point2i Vector2i;
typedef Point2f Vector2f;
typedef Point Vector;
typedef Point3i Vector3i;
typedef Point3f Vector3f;

// Узнать cos между двумя векторами
double getCos(Vector3f v1, Vector3f v2);

// Узнать длину вектора
double lenght(Vector3f v);

// Нормализация ветора
void normalize(Vector3f &v);

// Умножение вектора на число
void multi(Vector3f &v, double k);

// Векторное произведение
Vector3f multiVec(Vector3f a, Vector3f b);

// Вектора параллельны
bool    isParallel(Vector3f a, Vector3f b);
// Вектора перпендикулярны
bool    isOrto(Vector3f a, Vector3f b);

// Расстояние от точки до прямой
double distance(Point3f p, Point3f pivot, Vector3f v);

// Создать вектор на основе двух точек
Vector3f createVector3f(Point3f p1, Point3f p2);


#endif // VECTOR_H

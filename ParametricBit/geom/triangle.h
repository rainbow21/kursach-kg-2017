#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <container/ivnode.h>
#include <Canvas/canvaspoint.h>
#include <math.h>
#include "geom/vector.h"

// Шаблонный класс для треугольника. Могут быть точки разных типов.
template <class T> class Triangle
{
public:
    T p1, p2, p3;
    double area();
};

// Создание узла списка для треугольников <CanvasPoint>
ivNode(NodeTriangle, Triangle<CanvasPoint>);


template <class T> double Triangle<T>::area()
{
    Vector va = {p2.x-p1.x, p2.y-p1.y, p2.z-p1.z};
    Vector vb = {p3.x-p1.x, p3.y-p1.y, p3.z-p1.z};
    double a = sqrt(va.x*va.x + va.y*va.y + va.z*va.z);
    double b = sqrt(vb.x*vb.x + vb.y*vb.y + vb.z*vb.z);
    double COS = (va.x*vb.x + va.y*vb.y + va.z*vb.z)/(double)(a*b);
    double SIN = sqrt(1 - COS*COS);


    return 1/2.0 * b * a * SIN;
}

#endif // TRIANGLE_H

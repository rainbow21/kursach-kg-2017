#include "ivrotator.h"


// Вращение в плоскости XY
void ivRotator::Rotate2D_XY(ScenePoint &P, double alpha)
{
    double local_x = P.x;
    P.x = P.x*cos(alpha) - P.y*sin(alpha);
    P.y = local_x*sin(alpha) + P.y*cos(alpha);
    P.z = P.z;
}
// Вращение в плоскости XZ
void ivRotator::Rotate2D_XZ(ScenePoint &P, double alpha)
{
    double local_x = P.x;
    P.x = P.x * cos(alpha) + P.z * sin(alpha);
    P.y = P.y;
    P.z = -local_x * sin(alpha) + P.z * cos(alpha);
}
// Вращение в плоскости YZ
void ivRotator::Rotate2D_YZ(ScenePoint &P, double alpha)
{
    double local_y = P.y;
    P.x = P.x;
    P.y = P.y * cos(alpha) - P.z * sin(alpha);
    P.z = local_y * sin(alpha) + P.z * cos(alpha);
}


// Поворачивает точку по всем осям
void ivRotator::rotate3D(ScenePoint &P, Vector3f V)
{
    // Поворот вокуг Х
    ivRotator::Rotate2D_YZ(P, V.x * M_PI/180);
    // Поврот вокруг Y
    ivRotator::Rotate2D_XZ(P, V.y * M_PI/180);
    // Поврот вокруг Z
    ivRotator::Rotate2D_XY(P, V.z * M_PI/180);
}


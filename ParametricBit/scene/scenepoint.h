#ifndef SCENEPOINT_H
#define SCENEPOINT_H

#include "container/ivnode.h"
#include "geom/point.h"
#include <math.h>
/*
struct ScenePoint
{
    double x, y, z, k;
};
typedef struct ScenePoint ScenePoint;
*/
typedef Point3f ScenePoint;
ivNode(NodeScenePoint, ScenePoint);

// Расстояние между точками
double distance(ScenePoint p1, ScenePoint p2);


#endif // SCENEPOINT_H

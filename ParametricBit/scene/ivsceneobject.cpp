#include "scene/ivsceneobject.h"

ivSceneObject::ivSceneObject(ScenePoint p)
{
    pivotPoint.x = p.x;
    pivotPoint.y = p.y;
    pivotPoint.z = p.z;
}

ivSceneObject::ivSceneObject(double x, double y, double z)
{
    pivotPoint.x = x;
    pivotPoint.y = y;
    pivotPoint.z = z;
}

#include "scene/scenepoint.h"

// Расстояние между точками
double distance(ScenePoint p1, ScenePoint p2)
{
    double dx = p2.x-p1.x;
    double dy = p2.y-p1.y;
    double dz = p2.z-p1.z;

    return sqrt(dx*dx + dy*dy + dz*dz);
}

#ifndef IVMODEL_H
#define IVMODEL_H

#include "ivsceneobject.h"
#include "container/ivlist.h"
#include "container/ivnode.h"
#include "integer.h"    // Специальный файл для целых чисел. Работа со списками
#include "io/iosystem.h"
#include "geom/point.h"
#include "geom/edge.h"
#include "cast.h"
#include "geom/polygon.h"
#include "scene/ivscenemovable.h"
#include "container/ivlist.h"
#include <QColor>
#include "geom/plane.h"
#include "index.h"
#include "container/ivlist.h"
#include "scene/scenepoint.h"
#include "render/intersectinfo.h"
#include "geom/vector.h"
#include "color.h"


class ivModel :public ivSceneMovable
{
private:
    ivList(NodeScenePoint, next)    point;
    ivList(NodeIntegerEdge, next)   edge;
    double radius;              // Радиус сферической оболочки
    void updatePlane();         // Обновить информацию о гранях
public:
    ivList(NodePolygonIndex, next) plane;
public:
    ivModel(const char *filename);
    ScenePoint getPoint(int i) const;
    int     getNumPoint() const;
    void fprint(char *filename);
    Point3f getCenter() const;
    virtual ScenePoint getPivot();
    double getRadius() const; // Узнать радиус сферической оболочки
    InterInfo intersect(ScenePoint p, Vector3f v) const;
    // Получить внешнюю нормаль от грани
    Vector3f getOutNormal(int index) const;
    virtual void rotate(double angleX, double angleY, double angleZ);

    virtual void moveOn(Vector3f v);
    virtual void moveOn(double dx, double dy, double dz);

    virtual void move();
};

#endif // IVMODEL_H

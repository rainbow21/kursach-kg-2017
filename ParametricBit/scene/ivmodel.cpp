#include "scene/ivmodel.h"
#include <QMessageBox>
#include "container/ivnode.h"
#include "container/ivlist.h"


// Default Constructor
ivModel::ivModel(const char *filename) :ivSceneMovable(0,0,0)    // Ебаный костыль, чтобы компилятор не ругался. pivotPoint перепишется
{
    //QMessageBox::information(NULL, "sss", filename);
    FILE *f = fopen(filename, "r");
    char buf[STD_STR_LEN];
    Full(buf, STD_STR_LEN, '\0');

    // Находим блок POINT
    while (Compare_String((char*)"POINT", buf, strlen("POINT")))
        Read_string(f, buf, STD_STR_LEN);


    // Читаем блок POINT до END_POINT
    while (1)
    {
        int x,y,z;
        fscanf(f, "%d, %d, %d\n", &x, &y, &z);
        Point3i p = {x,y,z};
        NodeScenePoint nd = {{(double)p.x, (double)p.y, (double)p.z}, NULL};
        point.append(nd);
        Full(buf, STD_STR_LEN, '\0');
        IOSystem::getNextString(f, buf, STD_STR_LEN);
        if (!Compare_String((char*)"END_POINT", buf, strlen("END_POINT")))
            break;
    }

    // Ищем блок EDGE
    while (Compare_String(buf, (char*)"EDGE", strlen("EDGE")))
        Read_string(f, buf, STD_STR_LEN);

    // Читаем блок EDGE до END_EDGE
    while (1)
    {
        int v1, v2;
        fscanf(f, "%d, %d\n", &v1, &v2);
        NodeIntegerEdge nd;
        nd.value.v1 = v1;
        nd.value.v2 = v2;
        this->edge.append(nd);

        Full(buf, STD_STR_LEN, '\0');
        IOSystem::getNextString(f, buf, STD_STR_LEN);
        if (!Compare_String((char*)"END_EDGE", buf, strlen("END_EDGE")))
            break;
    }

    // Ищем блока PLANE
    while (Compare_String((char*)"PLANE", buf, strlen("PLANE")))
        Read_string(f, buf, STD_STR_LEN);
    // Читаем блок PLANE до END_PLANE
    ivList(NodeIndex, next) bufPol;
    while (1)
    {
        int var;
        char c;

        fscanf(f, "%d", &var);
        fscanf(f, "%c", &c);    // Разделитель или \n
        if (var == -1)
        {
            PolygonIndex pl(bufPol);
            // Фактически индекс в массиве поддерживаемых цветов
            int keyColor;
            fscanf(f, "%d", &keyColor);
            fscanf(f, "%c", &c);
            pl.setColor(getEnableColor(keyColor));



            NodePolygonIndex nd = {pl, NULL};
            plane.append(nd);

            while (bufPol.getNum())
                delete bufPol.cut();
        }
        else
        {

            NodeIndex nd;
            nd.value = var;
            bufPol.append(nd);
        }
        Full(buf, STD_STR_LEN, '\0');
        IOSystem::getNextString(f, buf, STD_STR_LEN);
        if (!Compare_String((char*)"END_PLANE", buf, strlen("END_PLANE")))
            break;
    }


    // Ищем блок PIVOT_POINT
    while (Compare_String((char*)"PIVOT_POINT", buf, strlen("PIVOT_POINT")))
        Read_string(f, buf, STD_STR_LEN);

    // Читаем одно число
    int index;
    fscanf(f, "%d\n", &index);
    pivotPoint = point[index]->value;
    fclose(f);
    // Радиус сферической оболочки
    radius = getRadius();

    fprint((char*)STD_OUTPUT);
}


void ivModel::fprint(char *filename)
{
    FILE *f = fopen(filename, "w");
    fprintf(f,"POINT:\n");
    for (int i = 0; i < this->point.getNum(); i++)
        fprintf(f,"%8.3lf %8.3lf %8.3lf\n", point[i]->value.x, point[i]->value.y, point[i]->value.z);
    fprintf(f, "\n");
    fprintf(f, "EDGE:\n");
    for (int i = 0; i < this->edge.getNum(); i++)
        fprintf(f, "%4d %4d\n", edge[i]->value.v1, edge[i]->value.v2);

    fprintf(f, "PLANE:\n");
    for (int i = 0; i < this->plane.getNum(); i++)
    {
        for (int j = 0; j < plane[i]->value.getSize(); j++)
        {

            ScenePoint p = point[plane[i]->value.getPointIndex(j)]->value;//plane[i]->value.point[j]->value;
            fprintf(f, "%8.3lf %8.3lf %8.3lf; ", p.x, p.y, p.z);
        }
        fprintf(f,"\n");
    }

    fprintf(f, "PIVOT:\n");
    fprintf(f, "%8.3lf %8.3lf %8.3lf\n", pivotPoint.x, pivotPoint.y, pivotPoint.z);
    fprintf(f, "RADIUS:\n");
    fprintf(f, "%8.3lf", radius);

    fclose(f);
}

Point3f ivModel::getCenter() const
{
    Point3f r = {0,0,0};
    for (int i = 0; i < point.getNum(); i++)
    {
        r.x += point[i]->value.x;
        r.y += point[i]->value.y;
        r.z += point[i]->value.z;
    }
    r.x /= (double)point.getNum();
    r.y /= (double)point.getNum();
    r.z /= (double)point.getNum();
    return r;
}

// -------------------
// Получить i-ую точку
ScenePoint ivModel::getPoint(int i) const
{
    return point[i]->value;
}

// --------------------
// Сколько всего точек?
int ivModel::getNumPoint() const
{
    return point.getNum();
}


ScenePoint ivModel::getPivot()
{
    return this->pivotPoint;
}

// ----------------------------------
// Узнать радиус сферической оболочки
double ivModel::getRadius() const
{
    double r = 0;
    ScenePoint center = getCenter();
    for (int i = 0; i < point.getNum(); i++)
    {
        double buf = distance(point[i]->value, center);
        if (buf > r)
            r = buf;
    }
    return r;
}

// ---------------------------------
// Вернуть цвет пересечения с прямой
InterInfo ivModel::intersect(ScenePoint p, Vector3f v) const
{
    // Проверяем сферическую оболочку
    double dist = distance(getCenter(), p, v);
    InterInfo res;
    res.intersect = false;

    if (dist < radius)
    {
        // Просмотреть все полигоны
        for (int i = 0; i < plane.getNum(); i++)
        {
            ivList(NodeScenePoint, next) polygon;
            // Сформировать полигон из нужных точек в списке (их индексы)
            for (int j = 0; j < plane[i]->value.getSize(); j++)
                polygon.append(*point[plane[i]->value.getPointIndex(j)]);

            Plane   pl = Plane::getPlane(polygon);      // Выделили отдельную плоскость
            Vector3f n = getOutNormal(i);               // Нашли внешнюю нормаль к ней

            // Если луч и нормаль перпендикулярны, то Plane не виден!
            // Если внешняя нормаль направлена не в сторону p (опрной точки), то она также не видна (она изнанка)
            if (!isOrto(v,n))
            {
                ScenePoint inter = pl.getIntersect(p, v);

                // Найти max, min по каждой координате
                double minX = polygon[0]->value.x;
                double minY = polygon[0]->value.y;
                double minZ = polygon[0]->value.z;
                double maxX = minX;
                double maxY = minY;
                double maxZ = minZ;

                for (int q = 0; q < polygon.getNum(); q++)
                {
                    ScenePoint pnt = polygon[q]->value;
                    if (pnt.x > maxX)
                        maxX = pnt.x;
                    if (pnt.x < minX)
                        minX = pnt.x;
                    if (pnt.y > maxY)
                        maxY = pnt.y;
                    if (pnt.y < minY)
                        minY = pnt.y;
                    if (pnt.z > maxZ)
                        maxZ = pnt.z;
                    if (pnt.z < minZ)
                        minZ = pnt.z;
                }


                if ((inter.x > minX || eq(inter.x, minX))\
                        && (inter.x < maxX || eq(inter.x, maxX))\
                        && (inter.y > minY || eq(inter.y, minY))\
                        && (inter.y < maxY || eq(inter.y, maxY))\
                        && (inter.z > minZ || eq(inter.z, minZ))\
                        && (inter.z < maxZ || eq(inter.z, maxZ)))
                {
                    res.intersect = true;
                    res.color = plane[i]->value.getColor();
                    return res;
                }
            }
        }
        return res;
    }
    return res;
}

// Получить внешнюю нормаль от грани
Vector3f ivModel::getOutNormal(int index) const
{
    /* Все грани заданы специальным образом: точки заданы в порядке обхода "против часовой стрелки"
     * Чтобы получить внешнюю нормаль, нужно умножить вектор [(0),(1)]x[(n-1),(0)]
    */
    int n = plane[index]->value.getSize();
    // Замена индексации. Работаем с индексами в общем спике точек (модели), а не в конкретной плоскости
    int indexP0 = plane[index]->value.getPointIndex(0);
    int indexP1 = plane[index]->value.getPointIndex(1);
    int indexPn = plane[index]->value.getPointIndex(n-1);
    // Формируем точки
    ScenePoint p0 = point[indexP0]->value;
    ScenePoint p1 = point[indexP1]->value;
    ScenePoint pn = point[indexPn]->value;

    Vector3f v1 = createVector3f(p0, p1);
    Vector3f v2 = createVector3f(pn, p0);

    return multiVec(v1,v2);
}

// --------------
// Поворот модели
void ivModel::rotate(double angleX, double angleY, double angleZ)
{
    Point3f c = getCenter();
    Vector3f v = {-c.x, -c.y, -c.z};
    moveOn(v);
    // TO DO !!!
    v = {c.x, c.y, c.z};
    moveOn(v);
}

// Перемещение модели
void ivModel::moveOn(double dx, double dy, double dz)
{
    for (int i = 0; i < point.getNum(); i++)
    {
        point[i]->value.x += dx;
        point[i]->value.y += dy;
        point[i]->value.z += dz;
    }
}

void ivModel::moveOn(Vector3f v)
{
    moveOn(v.x, v.y, v.z);
}

void ivModel::move()
{
    moveOn(d.x, d.y, d.z);
}

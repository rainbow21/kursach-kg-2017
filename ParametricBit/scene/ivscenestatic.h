#ifndef IVSCENESTATIC_H
#define IVSCENESTATIC_H

#include "scene/ivsceneobject.h"

// Статический объект в сцене. Такой объект создать уже вполне можно. Однако он будет неизменяемым.
class ivSceneStatic :public ivSceneObject
{
private:
public:
    ivSceneStatic(ScenePoint p);
    ivSceneStatic(double x, double y, double z);
    virtual ScenePoint getPivot();
};

#endif // IVSCENESTATIC_H

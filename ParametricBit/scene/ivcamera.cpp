#include "scene/ivcamera.h"
#include "geom/ivrotator.h"
ivCamera::ivCamera(ScenePoint pivot, ScenePoint a, ScenePoint b, ScenePoint c, ScenePoint d, int dd) :ivSceneMovable(pivot)
{
    this->dist = dd;
    this->p1 = a;
    this->p2 = b;
    this->p3 = c;
    this->p4 = d;
    this->pivotPoint = pivot;
}

// --------------------
// Узнать опорную точку
ScenePoint ivCamera::getPivot()
{
    return pivotPoint;
}

// --------------------------
// Узнать картинную плоскость
Plane ivCamera::getImagePlane() const
{
    Plane p(p1, p2, p3);
    return p;
}

// ----------------------------------
// Узнать геометрические размеры окна
int ivCamera::getWidth() const
{
    return width;
}

int ivCamera::getHeight() const
{
    return height;
}

// ----------------------------------------------
// Узнать координаты пикселя в пространстве сцены
ScenePoint ivCamera::getPixelCoord(int i, int j) const
{
    // Какая часть по горизонтали и по вертикали
    double ki = height/(double)i;
    double kj = width/(double)j;
    // Вектора
    Vector3f Vx = {p2.x-p1.x, p2.y-p1.y, p2.z-p1.z};
    Vector3f Vy = {p4.x-p1.x, p4.y-p1.y, p4.z-p1.z};
    // Вектор пикселя по горизонтали
    Vx.x /= kj;
    Vx.y /= kj;
    Vx.z /= kj;
    // Вектор пикселя по вертикали
    Vy.x /= ki;
    Vy.y /= ki;
    Vy.z /= ki;
    // Сдвигаемся от p1 на найденный вектор
    ScenePoint r = p1;
    r.x += Vx.x + Vy.x;
    r.y += Vx.y + Vy.y;
    r.z += Vx.z + Vy.z;
    return r;
}

// ---------------------------------------------------------------
// Узнать пиксель, соответствующий данной точке в плоскости экрана
CanvasPoint ivCamera::getPixel(ScenePoint p) const
{
    CanvasPoint res = {-1,-1};
    if (pointIn(p))
    {
        Vector3f Vy = {p4.x-p1.x, p4.y-p1.y, p4.z-p1.z};
        Vector3f Vx = {p2.x-p1.x, p2.y-p1.y, p2.z-p1.z};
        Vector3f v = {p.x-p1.x, p.y-p1.y, p.z-p1.z};
        double prX = fabs(lenght(v)*getCos(v,Vx));
        double prY = fabs(lenght(v)*getCos(v,Vy));
        res.x = prX;
        res.y = prY;
        return res;
    }
    return res;
}

// Узнать проек

// -------------------------------
// Узнать координаты центра экрана
ScenePoint ivCamera::getDisplayCenter() const
{
    return getPixelCoord(width/2, height/2);
}

// ----------------------------
// Лежит ли точка внутри экрана
bool ivCamera::pointIn(ScenePoint p) const
{
    double S = width*height;
    Triangle<ScenePoint> t;
    // 1
    t.p1 = p1;
    t.p2 = p2;
    t.p3 = p;
    double s1 = t.area();
    // 2
    t.p1 = p2;
    t.p2 = p3;
    t.p3 = p;
    double s2 = t.area();
    // 3
    t.p1 = p3;
    t.p2 = p4;
    t.p3 = p;
    double s3 = t.area();
    // 4
    t.p1 = p4;
    t.p2 = p1;
    t.p3 = p;
    double s4 = t.area();
    return (s1+s2+s3+s4 < S) || eq(s1+s2+s3+s4, S);
}
//----------------
// Движение камеры
void ivCamera::moveOn(double dx, double dy, double dz)
{
    p1.x += dx; p2.x += dx; p3.x += dx; p4.x += dx;
    p1.y += dy; p2.y += dy; p3.y += dy; p4.y += dy;
    p1.z += dz; p2.z += dz; p3.z += dz; p4.z += dz;
    pivotPoint.x += dx; pivotPoint.y += dy; pivotPoint.z += dz;
    emit updateCoord(pivotPoint.x, pivotPoint.y, pivotPoint.z);
}

void ivCamera::moveOn(Vector3f v)
{
    moveOn(v.x, v.y, v.z);
}

void ivCamera::move()
{
    moveOn(d.x, d.y, d.z);
}

void ivCamera::rotate(double angleX, double angleY, double angleZ)
{
    Vector3f rt = {angleX, angleY, angleZ};
    // Вращаем относительно 0,0,0
    ivRotator::rotate3D(p1, rt);
    ivRotator::rotate3D(p2, rt);
    ivRotator::rotate3D(p3, rt);
    ivRotator::rotate3D(p4, rt);
    ivRotator::rotate3D(pivotPoint, rt);

}

#ifndef IVSCENEOBJECT_H
#define IVSCENEOBJECT_H

/* Класс для представления объектов сцены.
 * У данного класса есть только опорная точка, которая характеризует его положение в пространстве.
 *
 *
*/
#include "scene/scenepoint.h"
#include <QObject>
#include <QMatrix3x3>

class ivSceneObject :public QObject
{
    Q_OBJECT
protected:
    ScenePoint pivotPoint;             // Опорная точка
public:
    ivSceneObject(ScenePoint p);
    ivSceneObject(double x, double y, double z);
    virtual ScenePoint getPivot() = 0;
};

#endif

#ifndef IVSCENEMOVABLE_H
#define IVSCENEMOVABLE_H

#include <scene/ivsceneobject.h>
#include <geom/vector.h>
#include <QMatrix4x4>

// Движимые/изменяемые объекты на сцене
class ivSceneMovable :public ivSceneObject
{
protected:
    Vector3f d;     // Стандартный вектор, на который возможно передвижение объекта
public:
    ivSceneMovable(double x, double y, double z, Vector3f v = {0,0,0});
    ivSceneMovable(ScenePoint p, Vector3f v = {0,0,0});

    virtual void rotate(double angleX, double angleY, double angleZ) = 0;

    virtual void moveOn(Vector3f v) = 0;
    virtual void moveOn(double dx, double dy, double dz) = 0;

    virtual void move() = 0;
};

#endif // IVSCENEMOVABLE_H

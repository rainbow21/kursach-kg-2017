#include "scene/ivscenestatic.h"
// -------------------------------------------------------------
// Constructor - Они точно такие же как и у родительского класса
ivSceneStatic::ivSceneStatic(double x, double y, double z):ivSceneObject(x,y,z){}
ivSceneStatic::ivSceneStatic(ScenePoint p):ivSceneObject(p){}

// ----------------------------------------------------------------
// Переопределяем функцию, чтобы перестать быть абстрактным классом
ScenePoint ivSceneStatic::getPivot()
{
    return pivotPoint;
}

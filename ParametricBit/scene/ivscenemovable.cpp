#include <scene/ivscenemovable.h>

ivSceneMovable::ivSceneMovable(ScenePoint p, Vector3f v):ivSceneObject(p)
{
    d.x = v.x;
    d.y = v.y;
    d.z = v.z;
}

ivSceneMovable::ivSceneMovable(double x, double y, double z, Vector3f v):ivSceneObject(x,y,z)
{
    d.x = v.x;
    d.y = v.y;
    d.z = v.z;
}

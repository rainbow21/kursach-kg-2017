#ifndef IVCAMERA_H
#define IVCAMERA_H
/*  Камера: 5 основных точек
 *  Опорная точка + 4 угла обзора (картинная плскость)
*/

#include "scene/scenepoint.h"
#include "geom/plane.h"
#include "cast.h"
#include "geom/triangle.h"
#include "double.h"
#include <stdio.h>
#include <QObject>
#include "scene/ivscenemovable.h"

class ivCamera :public ivSceneMovable
{
    Q_OBJECT
public:
    ScenePoint pivotPoint;
    ScenePoint p1,p2,p3,p4;
    int dist;
    int width = 500;
    int height = 500;
public:
    ivCamera(ScenePoint pivot, ScenePoint a, ScenePoint b, ScenePoint c, ScenePoint d, int dd);
    virtual ScenePoint  getPivot();
    Plane getImagePlane() const;
    int getWidth() const;
    int getHeight() const;
    // Узнать координаты пикселя на экране, в пространстве сцены
    ScenePoint  getPixelCoord(int i, int j) const;
    // Обратное преобразование. Узнаем положение пикселя.
    CanvasPoint getPixel(ScenePoint p) const;
    // Узнать координаты центра экрана
    ScenePoint getDisplayCenter() const;


    virtual void rotate(double angleX, double angleY, double angleZ);
    virtual void moveOn(Vector3f v);
    virtual void moveOn(double dx, double dy, double dz);
    virtual void move();

    bool pointIn(ScenePoint p) const;
signals:
    void updateCoord(double, double, double);
};

#endif // IVCAMERA_H

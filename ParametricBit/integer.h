#ifndef INTEGER_H
#define INTEGER_H
#include "container/ivnode.h"
class IntegerEdge
{
public:
    int v1 = 0;
    int v2 = 0;
};
ivNode(NodeIntegerEdge, IntegerEdge);
#endif // INTEGER_H

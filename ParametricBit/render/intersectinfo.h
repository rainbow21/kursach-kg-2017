#ifndef INTERSECTINFO_H
#define INTERSECTINFO_H

#include "scene/scenepoint.h"
#include <QColor>

struct InterInfo
{
    //ScenePoint point;
    QColor     color;
    bool    intersect;
};

#endif // INTERSECTINFO_H

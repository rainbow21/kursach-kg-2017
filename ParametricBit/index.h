#ifndef INDEX_H
#define INDEX_H

#include "container/ivnode.h"
#include "container/ivlist.h"
#include <QColor>

ivNode(NodeIndex, int);

class PolygonIndex
{
private:
    ivList(NodeIndex, next) point;  // Индексы точек, составляющих данный полигон
    QColor color;
public:
    PolygonIndex(ivList(NodeIndex, next) list, QColor color = QColor(255,255,255))
    {
        for (int i = 0; i < list.getNum(); i++) {
            NodeIndex n = {list[i]->value, NULL};
            point.append(n);
        }
        this->color = color;
    }

    QColor getColor() {
        return color;
    }
    void setColor(QColor c) {
        this->color = c;
    }

    int getSize()
    {
        return point.getNum();
    }

    // Получить индекс точки, составляющей данный полигон, в списке точек модели
    int getPointIndex(int index) {
        return point[index]->value;
    }
};

ivNode(NodePolygonIndex, PolygonIndex);

#endif // INDEX_H

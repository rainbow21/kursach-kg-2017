#ifndef CANVASPOINT_H
#define CANVASPOINT_H
#include <container/ivlist.h>
#include "geom/point.h"

struct CanvasPoint
{
    int x,y;
};
typedef struct CanvasPoint CanvasPoint;


// Создание узла списка, для точек Canvas
ivNode(NodeCanvasPoint, CanvasPoint);
// Объявление узла для списка полигонов
ivNode(NodePolygon, ivList(NodeCanvasPoint, next));

#endif // CANVASPOINT_H

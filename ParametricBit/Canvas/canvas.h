#ifndef CANVAS_H
#define CANVAS_H

#include <QWidget>
#include <QPainter>
#include <QImage>
#include <QTimer>
#include <Canvas/canvaspoint.h>
#include <Canvas/drawgeom.h>
#include <QMouseEvent>
#include <QBrush>
#include <exception.h>
#include "bezhe.h"
#include "geom/triangle.h"

class Canvas : public QWidget
{
    Q_OBJECT
private:
    QImage *image;
public:
    explicit Canvas(QWidget *parent = 0);
    ~Canvas();
    virtual void paintEvent(QPaintEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    void clear();
    // Заполнение экрана. По умолчанию белым.
    void fill(QColor c = QColor(255,255,255));
    void init();
    // Узнать геометрические размеры
    int getImageWidth();
    int getImageHeight();
    // Отметить точку
    void markPoint(CanvasPoint p, QColor c = QColor(0,0,0));

    // Нарисвать
    /*_*** DRAW ***_*/
    void drawLine(CanvasPoint p1, CanvasPoint p2, QColor c = QColor(0,0,0));
    void drawCircle(CanvasPoint center, int r, QColor c = QColor(0,0,0));
    void drawArc(CanvasPoint center, int r, double alpha1, double alpha2, QColor c = QColor(0,0,0));
    void drawEllipse(CanvasPoint center, int a, int b, QColor c = QColor(0,0,0));
    void drawRect(CanvasPoint p, int w, int h, QColor c = QColor(0,0,0));
    void drawPoint(CanvasPoint p, QColor c = QColor(0,0,0));
    void drawBezhe(CanvasPoint *array, int n, QColor c = QColor(0,0,0)) throw (Ex_NullPtr, Ex_FewPointBezhe);
    /*_*** DRAW FILL ***_*/
    void drawFillTriangle(CanvasPoint p0, CanvasPoint p1, CanvasPoint p2, QColor c = QColor(0,0,0));
    void drawFillTriangle(Triangle<CanvasPoint> triangle, QColor color = QColor(0,0,0));

    // Загрузить картинку
    /*_*** LOAD ***_*/
    void loadImage(char* filename, CanvasPoint p, QColor *transparent= NULL);
    void loadImage(QImage *img, CanvasPoint p, QColor *transparent = NULL);

signals:
    void mouseMove(int x, int y);
    void mousePress(int x, int y);
public slots:

private slots:

};

#endif // CANVAS_H

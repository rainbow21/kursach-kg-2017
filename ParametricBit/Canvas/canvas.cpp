#include "canvas.h"
#include <QMessageBox>
#include <QDebug>
#include <QIODevice>
#include <cast.h>


Canvas::Canvas(QWidget *parent) : QWidget(parent)
{
    setMouseTracking(true);
}

Canvas::~Canvas()
{
    delete image;
}

void Canvas::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter Painter(this);
    Painter.setBrush(Qt::BrushStyle::NoBrush);
    Painter.drawImage(0,0,*image);
    Painter.end();
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    emit mouseMove(event->pos().x(), event->pos().y());
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    emit mousePress(event->pos().x(), event->pos().y());
}

void Canvas::clear()
{
    fill(QColor(0,0,0));
}

void Canvas::fill(QColor c)
{

//    for (int i = 0; i < width(); i++)
//        for (int j = 0; j < height(); j++)
//            image->setPixelColor(i, j, c);

    QPainter painter(image);
    QRect rect;
    rect.setWidth(width());
    rect.setHeight(height());

    painter.fillRect(rect, c);


    painter.end();
}

void Canvas::init()
{
    image = new QImage(width(), height(), QImage::Format::Format_RGB16);
    clear();
}

// ----------------------
// Узнать ширину картинки
int Canvas::getImageWidth()
{
    return image->width();
}

// ----------------------
// Узнать высоту картинки
int Canvas::getImageHeight()
{
    return image->height();
}

// --------------
// Отметить точку
void Canvas::markPoint(CanvasPoint p, QColor c)
{
    drawCircle(p, 5, c);
}

// ----------------
// Нарисовать линию
void Canvas::drawLine(CanvasPoint p1, CanvasPoint p2, QColor c)
{
    //DrawLineBrezenhemInt(image, p1, p2, c);
    DrawLineBrezenhemInt(image, p1, p2, c);
}

// ---------------------
// Нарисовать окружность
void Canvas::drawCircle(CanvasPoint center, int r, QColor c)
{
    DrawCircleCanonEq(image, center, r, c);
}

// ----------------------------------
// Нарисовать дугу. Часть окружности.
void Canvas::drawArc(CanvasPoint center, int r, double alpha1, double alpha2, QColor c)
{
    if (alpha1 < alpha2)
        DrawArc(image, center, r, alpha1, alpha2, c);
    else
    {
        DrawArc(image, center, r, alpha1, 360, c);
        DrawArc(image, center, r, 0, alpha2, c);
    }
}

// -----------------
// Нарисовать эллипс
void Canvas::drawEllipse(CanvasPoint center, int a, int b, QColor c)
{
    DrawEllipseCanonEq(image, center, a, b, c);
}

// ----------------
// Нарисовать точку
void Canvas::drawPoint(CanvasPoint p, QColor c)
{
    image->setPixelColor(p.x, p.y, c);
}

// -----------------------
// Нарисовать кривую Безье
void Canvas::drawBezhe(CanvasPoint *array, int n, QColor c) throw (Ex_NullPtr, Ex_FewPointBezhe)
{
    if (!array) throw(Ex_NullPtr("Null Ptr in \"Canvas::drawBezhe\""));
    if (n < 3) throw(Ex_FewPointBezhe("Few point for Bezhe in \"Canvas::drawBezhe\""));
    double t = 0;
    double eps = 0.0001;
    double dt = 0.02;

    CanvasPoint p0 = pointBezhe(array, n, 0);
    CanvasPoint p;
    while (fabs(t-1) > eps)
    {
        p = pointBezhe(array, n, t);
        drawLine(p0, p, c);
        p0 = p;
        t += dt;
    }

    p = pointBezhe(array, n, 1);
    drawLine(p0, p, c);

}

// ----------------------------------
// Нарисовать заполненный треугольник
void Canvas::drawFillTriangle(CanvasPoint p0, CanvasPoint p1, CanvasPoint p2, QColor c)
{
    if (p0.y == p1.y && p0.y == p2.y) return;
    if (p0.y > p1.y) std::swap(p0, p1);
    if (p0.y > p2.y) std::swap(p0, p2);
    if (p1.y > p2.y) std::swap(p1, p2);

    int total_height = p2.y - p0.y;
    for (int i = 0; i < total_height; i++)
    {
        bool secondHalf = i > p1.y-p0.y || p1.y == p0.y;
        int segmentHalf = secondHalf ? p2.y-p1.y : p1.y-p0.y;
        double alpha = (double)i/total_height;
        double beta = (double)(i - (secondHalf ? p1.y-p0.y : 0))/segmentHalf;

        CanvasPoint A = {(int)(p0.x + (p2.x-p0.x)*alpha), (int)(p0.y + (p2.y-p0.y)*alpha)};
        CanvasPoint B = {(int)(secondHalf ? p1.x + (p2.x-p1.x)*beta : p0.x + (p1.x-p0.x)*beta),
                         (int)(secondHalf ? p1.y + (p2.y-p1.y)*beta : p0.y + (p1.y-p0.y)*beta)};
        if (A.x > B.x) std::swap(A, B);
        for (int j = A.x; j <= B.x; j++)
            image->setPixelColor(j, p0.y + i, c);

    }
}

// ----------------------------------
// Нарисовать заполненный треугольник
void Canvas::drawFillTriangle(Triangle<CanvasPoint> triangle, QColor color)
{
    drawFillTriangle(triangle.p1, triangle.p2, triangle.p3, color);
}

// ------------------------
// Нарисовать прямоугольник
void Canvas::drawRect(CanvasPoint p, int w, int h, QColor c)
{
    CanvasPoint p2 = {p.x + w, p.y};
    CanvasPoint p3 = {p.x + w, p.y + h};
    CanvasPoint p4 = {p.x, p.y + h};
    drawLine(p, p2, c);
    drawLine(p2, p3, c);
    drawLine(p3, p4, c);
    drawLine(p4, p, c);
}

// ------------------
// Загрузить картинку
void Canvas::loadImage(char *filename, CanvasPoint p, QColor *transparent)
{
    QImage *buf = new QImage(filename);
    loadImage(buf, p, transparent);
    delete buf;
}

void Canvas::loadImage(QImage *img, CanvasPoint p, QColor *transparent)
{
    int b_x = 0;

    for (int x = p.x; x < image->width() && b_x < img->width(); x++, b_x++)
    {
        int b_y = 0;
        for (int y = p.y; y < image->height() && b_y < img->height(); y++, b_y++)
        {
            QColor c = QColor(img->pixelColor(b_x, b_y));
            if (!transparent || *transparent != c)
                image->setPixelColor(x, y, c);
        }
    }
}

#include <Canvas/drawgeom.h>

// Рисование окружности по каноническому уравнению
void DrawCircleCanonEq(QImage *Sc, CanvasPoint c, int r, QColor color)
{
    int xm = r/sqrt(2.0);
    for (int x = 0; x <= xm; x++)
    {
        int y = (int) (sqrt((double)r*r - x*x) + 0.5);
        //отрисовываем четыре симметричных пикселя
        Sc->setPixelColor(c.x+x, c.y+y, color);
        Sc->setPixelColor(c.x+x, c.y-y, color);
        Sc->setPixelColor(c.x-x, c.y+y, color);
        Sc->setPixelColor(c.x-x, c.y-y, color);
    }

    int ym = (int) (sqrt((double)r*r - xm*xm) + 0.5);
    for (int y = 0; y <= ym; y++)
    {
        int x = (int) (sqrt((double)r*r - y*y) + 0.5);
        //отрисовываем четыре симметричных пикселя
        Sc->setPixelColor(c.x+x, c.y+y, color);
        Sc->setPixelColor(c.x+x, c.y-y, color);
        Sc->setPixelColor(c.x-x, c.y+y, color);
        Sc->setPixelColor(c.x-x, c.y-y, color);
    }
}


void DrawLineBrezenhemInt(QImage *Sc, CanvasPoint p1, CanvasPoint p2, QColor Color)
{
        int x1 = round(p1.x);
        int y1 = round(p1.y);
        int x2 = round(p2.x);
        int y2 = round(p2.y);

        //если отрезок вырожденный, то отрисовываем только одну точку
        if (x2 == x1 && y2 == y1)
        {
            //обрабатываем ТОЛЬКО пиксели внутри массива битмапа
            if (!((x1 >= Sc->width()) || (x1 < 0) || (y1 >= Sc->height()) || (y1 < 0)))
                //Sc->addPoint(x1, y1, Color);
                Sc->setPixelColor(x1, y1, Color);
        }
        else
        {
            int dx=x2-x1, dy=y2-y1; //приращение координат
            int sx = SIGN(dx), sy = SIGN(dy); //шаг по X и по Y
            dx = ABS(dx); dy = ABS(dy); //абсолютируем приращения

            bool swap; //флаг обмена
            if (dy <= dx)
                swap = false;
            else
            {
                swap = true;
                int t = dx;
                dx = dy;
                dy = t;
            }

            int _E = 2*dy - dx; //ошибка
            int xt=x1, yt=y1; //текущие координаты

            //в цикле анализируем ошибку
            for (int i=1; i<=dx+1; i++)
            {
                //обрабатываем ТОЛЬКО пиксели внутри массива сцены
                if (!((xt >= Sc->width()) || (xt < 0) || (yt >= Sc->height()) || (yt < 0)))
                    //Sc->addPoint(xt,yt, Color);
                    Sc->setPixelColor(xt, yt, Color);
                if (_E>=0)
                {
                    if (swap)
                        xt += sx;
                    else
                        yt += sy;
                    _E = _E - 2*dx;
                }
                if (_E<0)
                {
                    if (swap)
                        yt += sy;
                    else
                        xt += sx;
                }
                _E = _E + 2*dy;
            }
        }
}

// Рисование эллипса с помощью канонического уравнения
void DrawEllipseCanonEq(QImage *Sc, CanvasPoint center, int a, int b, QColor c)
{
    int xm = a*a/sqrt(a*a + b*b);

    for (int x = 0; x <= xm; x++)
    {
        int y = (int) (b * sqrt(1.0 - x*x/double(a*a)) + 0.5);
        //отрисовываем четыре симметричных пикселя
        Sc->setPixelColor(center.x+x, center.y+y, c);
        Sc->setPixelColor(center.x+x, center.y-y, c);
        Sc->setPixelColor(center.x-x, center.y+y, c);
        Sc->setPixelColor(center.x-x, center.y-y, c);
    }
    int ym = (int) (b * sqrt(1.0 - xm*xm/double(a*a)) + 0.5);
    for(int y = 0; y <= ym; y++)
    {
        int x = (int) (a * sqrt(1.0 - y*y/double(b*b)) + 0.5);
        //отрисовываем четыре симметричных пикселя
        Sc->setPixelColor(center.x+x, center.y+y, c);
        Sc->setPixelColor(center.x+x, center.y-y, c);
        Sc->setPixelColor(center.x-x, center.y+y, c);
        Sc->setPixelColor(center.x-x, center.y-y, c);
    }
}


// Рисование дуги
void DrawArc(QImage *Sc, CanvasPoint center, int r, double alpha1, double alpha2, QColor color)
{
    ///x = R * cos(A) + X
    ///y = R * sin(A) + Y
    int power = 10;
    double eps = 0.0001;
    double step_a = (alpha2 - alpha1)/(r*power);

    for (double a = alpha1; fabs(alpha2-a) > eps; a += step_a)
    {
        int x = round(r * cos(a*M_PI/180) + center.x);
        int y = round(r * sin(a*M_PI/180) + center.y);
        if (x > 0 && x < Sc->width() && y > 0 && y < Sc->height())
            Sc->setPixelColor(x, y, color);
    }
}

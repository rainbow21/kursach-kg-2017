#ifndef DRAWGEOM_H
#define DRAWGEOM_H

#include <QColor>
#include <QImage>
#include <Canvas/iv_math.h>
#include <Canvas/canvaspoint.h>



// Рисование окружности по каноническому уравнению
void DrawCircleCanonEq(QImage *Sc, CanvasPoint c, int r, QColor color = QColor(0,0,0));

// Рисование отрезка алгоритмом Брезенхема
void DrawLineBrezenhemInt(QImage *Sc, CanvasPoint p1, CanvasPoint p2, QColor Color);

// Рисование Эллипса
void DrawEllipseCanonEq(QImage *Sc, CanvasPoint center, int a, int b, QColor c);

// Рисование дуги
void DrawArc(QImage *Sc, CanvasPoint center, int r, double alpha1, double alpha2, QColor color = QColor(0,0,0));

#endif // DRAWGEOM_H

#ifndef IV_MATH_H
#define IV_MATH_H


#define INT_PRINT(Var) printf(#Var " = %d;\n", Var);
#define FLT_PRINT(Var) printf(#Var " = %lf0.2;\n", Var);

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define SIGN(x) ( (x) < 0 ? (-1) : ((x)==0 ? 0 : (1)) )
#define ABS(x) ( (x) < 0 ? (x)*(-1) : (x) )

#endif // IV_MATH_H

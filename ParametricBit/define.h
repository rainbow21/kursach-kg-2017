#ifndef DEFINE_H
#define DEFINE_H

#define STD_STR_LEN 100
#define STD_INPUT   "in.txt"
#define STD_OUTPUT  "out.txt"

#define PRINT_POINT3F(p)\
    printf("%8.3lf %8.3lf %8.3lf\n", p.x, p.y, p.z)

#define EPS 0.0001

#define UNUSED(var) (void*)var

#endif // DEFINE_H

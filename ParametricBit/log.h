#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <ctime>
#include "windows.h"

#define STD_LOG "../../log/log.txt"

// Записать сообщение
void log(const char *filename, const char *txt);
// Записать сообщение с отметкой времни (полный формат)
void logLongTime(const char *filename, const char *txt);
// Записать сообщение с отметкой времни (короткий формат)
void logTime(const char *filename, const char *txt);
// Очистить всю историю сообщений
void logClear(const char *filename);

#endif // LOG_H

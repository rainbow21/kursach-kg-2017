#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <stdexcpt.h>
// Общее исключение
class UniversalException :public std::exception
{
private:
    const char *message;
public:
    UniversalException(const char *msg) :message(msg) {}
    virtual const char* what() const throw() override
    {
        return message;
    }
};
// Некорректный индекс
class Ex_InvalidIndex :public UniversalException
{
public:
    Ex_InvalidIndex(const char *msg):UniversalException(msg) {}
};
// Ошибка выделения памяти
class Ex_MemoryError :public UniversalException
{
public:
    Ex_MemoryError(const char *msg):UniversalException(msg) {}
};
// Нулевой указатель
class Ex_NullPtr :public UniversalException
{
public:
    Ex_NullPtr(const char *msg):UniversalException(msg) {}
};
// Слишком мало точек для алгоритма Безье
class Ex_FewPointBezhe :public UniversalException
{
public:
    Ex_FewPointBezhe(const char *msg):UniversalException(msg){}
};
// Слишком мало точек для треугольника
class Ex_FewPointTriangle :public UniversalException
{
public:
    Ex_FewPointTriangle(const char *msg):UniversalException(msg){}
};
// Недостаточно точек для триангуляции
class Ex_FewPointForTriangulate :public UniversalException
{
public:
    Ex_FewPointForTriangulate(const char *msg):UniversalException(msg){}
};

// Недостаточно точек для точек
class Ex_FewPlaneForEntity :public UniversalException
{
public:
    Ex_FewPlaneForEntity(const char *msg):UniversalException(msg){}
};

// Файл не найден
class Ex_FileNotFound :public UniversalException
{
public:
    Ex_FileNotFound(const char *msg):UniversalException(msg){}
};

#endif // EXEPTION_H

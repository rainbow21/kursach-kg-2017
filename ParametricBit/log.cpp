#include "log.h"

extern SYSTEMTIME sys_time;

// ------------------
// Записать сообщение
void log(const char *filename, const char *txt)
{
    FILE *f = fopen(filename, "a");
    fprintf(f, "%s\n", txt);
    fclose(f);
}

// -------------------------------------------------------
// // Записать сообщение с отметкой времни (полный формат)
void logLongTime(const char *filename, const char *txt)
{
    FILE *f = fopen(filename, "a");
    fprintf(f, "%s\t\t\t", txt);
    GetLocalTime(&sys_time);
    fprintf(f, "%d-%02d-%02d  %d:%d:%d.%d\n",
            sys_time.wYear, sys_time.wMonth, sys_time.wDay, sys_time.wHour, sys_time.wMinute, sys_time.wSecond, sys_time.wMilliseconds);
    fclose(f);
}


// ------------------------------------------------------
// Записать сообщение с отметкой времни (короткий формат)
void logTime(const char *filename, const char *txt)
{
    FILE *f = fopen(filename, "a");
    fprintf(f, "%s\t\t\t", txt);
    GetLocalTime(&sys_time);
    fprintf(f, "%d:%d:%d.%d\n", sys_time.wHour, sys_time.wMinute, sys_time.wSecond, sys_time.wMilliseconds);
    fclose(f);
}

// ------------------------------
// Очистить всю историю сообщений
void logClear(const char *filename)
{
    FILE *f = fopen(filename, "w");
    fclose(f);
}

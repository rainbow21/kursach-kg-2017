#include <cast.h>

Point2i cast(const CanvasPoint &x2)
{
    Point2i p = {x2.x, x2.y};
    return p;
}

CanvasPoint cast(const Point2i &x2)
{
    CanvasPoint p = {x2.x, x2.y};
    return p;
}


Point3f         cast(const ScenePoint   &p)
{
    Point3f r = {p.x, p.y, p.z};
    return r;
}

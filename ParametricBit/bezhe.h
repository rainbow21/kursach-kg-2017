#ifndef BEZHE_H
#define BEZHE_H
#include <Canvas/canvaspoint.h>
#include <geom/vector.h>

// Функция получает точку кривой безье
CanvasPoint pointBezhe(CanvasPoint *array, int n, double t);

#endif // BEZHE_H

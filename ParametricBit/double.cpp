#include "double.h"

bool eq(double a, double b)
{
    return fabs(a-b) <= EPS;
}

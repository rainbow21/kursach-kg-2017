#ifndef CONENT_H
#define CONENT_H

#include "container/ivlist.h"
#include "geom/point.h"
#include "scene/ivcamera.h"
#include "scene/ivmodel.h"

// Структура для содержимого главного окна
struct Content
{
    ivList(NodePoint2i, next)   listPoint;
    ivCamera                    *camera;
    ivModel                     *model;
};
typedef struct Content Content;

#endif // CONENT_H

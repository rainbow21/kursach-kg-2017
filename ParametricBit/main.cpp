#include "mainwindow.h"
#include <QApplication>
#include "log.h"
#include <windows.h>
#include <QStyleFactory>

SYSTEMTIME sys_time;

int main(int argc, char **argv)
{
    logLongTime(STD_LOG, "\n\nStart new Program");

    QApplication a(argc, argv);
    qApp->setStyle(QStyleFactory::create("Fusion"));
    MainWindow w;
    w.show();

    return a.exec();
}


/* Если что, тут лежат альтернативные (и более дубовые) варианты по получению времни
 *  QString s("\n\nStart new Program ");
    log(STD_LOG, s.toStdString().c_str());
    Или
    QString s("\n\nStart new Program ");
    s.append(QString(ctime(&prg_time)));
    log(STD_LOG, s.toStdString().c_str());
 */

/*
 * Программа изображения дифформируемых трехмерных объектов.
 * Программа моделирования трехмерных дифформируемых объектов.
 *
 * */
